import './App.css';
import Button from 'react-bootstrap/Button';
import { useState } from 'react';

import logo from "./logo.svg";
import "./App.css";
import CustomInput from "./CustomInput";
import CustomButton from "./CustomButton";
import { FormCheck } from 'react-bootstrap';

function App() {
  return (
    <div className="App " >
      <header className="App-header">
        <div className='mb-5'>

        <h1>Nuevo Usuario</h1>
        </div>
        
        <CustomInput label={"Usuario"} disabled={false} />
        <CustomInput label={"Correo"} disabled={false}/>
        <CustomInput label={"Contraseña"} disabled={false}/>
        <div className='mt-3'>

        <label htmlFor="" className='ml-3'> 
        <input class="form-check-input" />
           Acepto terminos y condiciones</label>
        </div>
      
        <div className="mt-3  " >
          <CustomButton name={"Aceptar"} disabled={false} />
          <CustomButton name={"Cancelar"} disabled={false} />
        </div>
      </header>
    </div>
  );
}


export default App;